# Komputer store

Komputer store is dynamic webpage created with HTML, CSS and "vanilla" JavaScript as an exercise. It lets the user earn money and take loans to buy laptops (and not only laptops). 

## Features

-   See your bank balance
-   Take a loan (one at a time and not more than double your balance)
-   See the amount of debt you have
-   View your salary and send it to the bank account (10% goes to pay back loans)
-   Pay back the loan with full salary
-   Work to make more money
-   Select laptops from a menu and view their info
-   Buy a laptop if you have enough balance

## Install & run instructions

- Download all files
- Launch index.html in your browser

## Example image

![](komputer-store-screenshot.jpg)