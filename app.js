const balanceElement = document.getElementById("balance");
const loanButtonElement = document.getElementById("loanButton");
const loanAmountElement = document.getElementById("loanAmount");
const showLoanElement = document.getElementById("showLoan");

const salaryElement = document.getElementById("salary");
const repayLoanButtonElement = document.getElementById("repayLoanButton");
const showRepayElement = document.getElementById("showRepay");
const bankButtonElement = document.getElementById("bank");
const workButtonElement = document.getElementById("work");

const laptopsElement = document.getElementById("laptops");
const featuresElement = document.getElementById("features");
const imageElement = document.getElementById("image");
const titleElement = document.getElementById("laptopTitle");
const descriptionElement = document.getElementById("laptopDescription");
const priceElement = document.getElementById("laptopPrice");
const buyButtonElement = document.getElementById("buyButton");

// Bank box
let balance = 0;
let loan = 0;
balanceElement.innerHTML = balance;
loanAmountElement.innerHTML = loan;

const showLoanAmount = () => {
    if (loan <= 0) {
        showLoanElement.style.visibility = "hidden";
      } else {
        showLoanElement.style.visibility = "visible";
      }
}
showLoanAmount();



loanButtonElement.onclick = () => {
    if(loan > 0) {
        alert("You need to pay back your old loan first :(")
    } else {
        const loanRequest = parseInt(prompt('Enter loan amount'));
        if(loanRequest > balance * 2) {
            alert("You can't get this much loan right now :(")
        } else {
            loan += loanRequest;
            balance += loan;
            balanceElement.innerHTML = balance;
            loanAmountElement.innerHTML = loan;
            showLoanAmount();
            showRepay();
        }
    }
}

// Work box
let salary = 0;
salaryElement.innerHTML = salary;

const showRepay = () => {
    if (loan <= 0) {
        showRepayElement.style.visibility = "hidden";
      } else {
        showRepayElement.style.visibility = "visible";
      }
}
showRepay();

repayLoanButtonElement.onclick = () => {
    if(loan >= salary) {
        loan -= salary;
        salary = 0;
    } else if(salary > loan) {
        salary -= loan;
        loan = 0;
    }
    showLoanAmount();
    showRepay();
    balanceElement.innerHTML = balance;
    salaryElement.innerHTML = salary;
    loanAmountElement.innerHTML = loan;
}

bankButtonElement.onclick = () => {
    if(loan >= salary * 0.1) {
        loan -= salary * 0.1;
        balance += salary * 0.9;
        salary = 0;
    } else if (loan > 0 && loan < salary * 0.1) {
        salary -= loan;
        loan = 0;
        balance += salary;
        salary = 0;
    } else {
        balance += salary;
        salary = 0;
    }
    showLoanAmount();
    showRepay();
    balanceElement.innerHTML = balance;
    salaryElement.innerHTML = salary;
    loanAmountElement.innerHTML = loan;
}

workButtonElement.onclick = () => {
    salary += 100;
    salaryElement.innerHTML = salary;
}


// Laptop boxes
let laptops = [];
const baseUrl = "https://noroff-komputer-store-api.herokuapp.com/";

fetch(baseUrl + "computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => {
        laptops.forEach(laptop => {
            const laptopElement = document.createElement("option");
            laptopElement.value = laptop.id;
            laptopElement.appendChild(document.createTextNode(laptop.title));
            laptopsElement.appendChild(laptopElement);
        })
        featuresElement.innerHTML = laptops[0].specs;
        imageElement.src = baseUrl + laptops[0].image;
        titleElement.innerHTML = laptops[0].title;
        descriptionElement.innerHTML = laptops[0].description;
        priceElement.innerHTML = laptops[0].price;
    })

const handleLaptopMenuChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    featuresElement.innerHTML = selectedLaptop.specs;
    imageElement.src = baseUrl + selectedLaptop.image;
    titleElement.innerHTML = selectedLaptop.title;
    descriptionElement.innerHTML = selectedLaptop.description;
    priceElement.innerHTML = selectedLaptop.price;
}

laptopsElement.addEventListener("change", handleLaptopMenuChange);

buyButtonElement.onclick = () => {
    if(priceElement.innerText > balance) {
        alert(`You can't afford ${titleElement.innerText} at the moment :(`);
    } else {
        balance -= priceElement.innerText;
        alert(`Congratulations! You are now the owner of ${titleElement.innerText} <3`)
    }
    balanceElement.innerHTML = balance;
}